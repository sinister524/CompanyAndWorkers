package ru.aisa.companyAndWorkers.services;

import lombok.extern.log4j.Log4j2;
import org.springframework.dao.DuplicateKeyException;
import ru.aisa.companyAndWorkers.dao.WorkerDao;
import ru.aisa.companyAndWorkers.entity.Company;
import ru.aisa.companyAndWorkers.entity.Worker;
import ru.aisa.companyAndWorkers.exception.NotUniqueValueException;
import ru.aisa.companyAndWorkers.repository.WorkerRepository;

import java.util.List;
import java.util.NoSuchElementException;

@Log4j2
public class WorkerService {

    private static WorkerService instance;

    private final WorkerRepository repository;

    private WorkerService() {
        this.repository = WorkerDao.getInstance();
    }

    public static synchronized WorkerService getInstance() {
        if (instance == null) {
            instance = new WorkerService();
        }
        return instance;
    }

    public List<Worker> findAll() {
        log.debug("Loading all workers...");
        return repository.findAll();
    }

    public List<Worker> findAllCompanyWorkers(Company company) {
        log.debug("Loading company " + company.toString() + " workers...");
        return repository.findAllCompanyWorkers(company);
    }

    public Worker findById(Long id) {
        log.debug("Searching worker by ID = " + id + "...");
        return repository.findById(id).orElseThrow(NoSuchElementException::new);
    }

    public Worker findByEmail(String email) {
        log.debug("Searching worker by email = " + email + "...");
        return repository.findByEmail(email).orElseThrow(NoSuchElementException::new);
    }

    public Worker save(Worker worker) {
        log.debug("Creating worker - " + worker.getName() + "...");
        try {
            return repository.save(worker).orElseThrow(NoSuchElementException::new);
        } catch (DuplicateKeyException e) {
            log.error("Worker with email " + worker.getEmail() + " already exists!");
            throw new NotUniqueValueException();
        }
    }

    public Worker update(Worker worker) {
        log.debug("Updating worker with ID = " + worker.getId() + "...");
        try {
            return repository.update(worker).orElseThrow(NoSuchElementException::new);
        } catch (DuplicateKeyException e){
            log.error("Worker with email " + worker.getEmail() + " already exists!");
            throw new NotUniqueValueException();
        }
    }

    public void delete(Long id) {
        log.debug("Deleting worker with ID = " + id + "...");
        findById(id);
        repository.delete(id);
    }

    public void delete(String email) {
        log.debug("Deleting worker with email = " + email + "...");
        findByEmail(email);
        repository.delete(email);
    }
}
