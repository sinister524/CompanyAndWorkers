package ru.aisa.companyAndWorkers.services;

import lombok.extern.log4j.Log4j2;
import org.springframework.dao.DuplicateKeyException;
import ru.aisa.companyAndWorkers.dao.CompanyDao;
import ru.aisa.companyAndWorkers.entity.Company;
import ru.aisa.companyAndWorkers.exception.NotUniqueValueException;
import ru.aisa.companyAndWorkers.repository.CompanyRepository;

import java.util.List;
import java.util.NoSuchElementException;

@Log4j2
public class CompanyService {

    private static CompanyService instance;

    private final CompanyRepository repository;

    private CompanyService() {
        this.repository = CompanyDao.getInstance();
    }

    public static CompanyService getInstance() {
        if (instance == null) {
            instance = new CompanyService();
        }
        return instance;
    }

    public List<Company> findAll(){
        log.debug("Loading all companies...");
        return repository.findAll();
    }

    public Company findById(Long id) {
        log.debug("Searching company by ID = " + id + "...");
        return repository.findById(id).orElseThrow(NoSuchElementException::new);
    }

    public Company findByInn(String inn) {
        log.debug("Searching company by INN = " + inn + "...");
        return repository.findByInn(inn).orElseThrow(NoSuchElementException::new);
    }

    public Company save(Company company) {
        log.debug("Creating company " + company.getName() + "...");
        try {
            return repository.save(company).orElseThrow(NoSuchElementException::new);
        } catch (DuplicateKeyException e) {
            log.error("Company with INN = " + company.getInn() + " already exists!");
            throw new NotUniqueValueException();
        }
    }

    public Company update(Company company){
        log.debug("Updating company with ID = " + company.getId() + "...");
        try {
            return repository.update(company).orElseThrow(NoSuchElementException::new);
        } catch (DuplicateKeyException e) {
            log.error("Company with INN = " + company.getInn() + " already exists!");
            throw new NotUniqueValueException();
        }
    }

    public void delete(Long id) {
        log.debug("Deleting company with ID = " + id + "...");
        findById(id);
        repository.delete(id);
    }

    public void delete(String inn) {
        log.debug("Deleting company with INN = " + inn + "...");
        findByInn(inn);
        repository.delete(inn);
    }
}
