package ru.aisa.companyAndWorkers.comfig;


import com.vaadin.server.VaadinService;
import lombok.extern.log4j.Log4j2;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Log4j2
public class SpringJdbcConfig {

    private static SpringJdbcConfig instance;

    private SpringJdbcConfig() {
        log.debug("Creating SpringJdbcConfig");
        initProperties();
    }

    private String bdUrl;
    private String username;
    private String password;

    public DataSource postgreDataSource() {
        log.debug("Getting DataSource");
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl(bdUrl);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }

    private void initProperties(){
        log.debug("Initializing properties");
        String basePath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
        try(FileInputStream fileInputStream = new FileInputStream(basePath + "/resources/application.properties")) {
            Properties properties = new Properties();
            properties.load(fileInputStream);
            this.bdUrl = properties.getProperty("bd.url");
            this.username = properties.getProperty("bd.username");
            this.password = properties.getProperty("bd.password");
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public static synchronized SpringJdbcConfig getInstance() {
        log.debug("Getting JDBC Config singleton object");
        if (instance == null){
            instance = new SpringJdbcConfig();
        }
        return instance;
    }
}
