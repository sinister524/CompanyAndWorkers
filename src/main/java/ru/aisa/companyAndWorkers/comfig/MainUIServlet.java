package ru.aisa.companyAndWorkers.comfig;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinServlet;
import ru.aisa.companyAndWorkers.view.MainView;

import javax.servlet.annotation.WebServlet;

@WebServlet(urlPatterns = "/*", asyncSupported = true)
@VaadinServletConfiguration(ui = MainView.class, productionMode = false)
public class MainUIServlet extends VaadinServlet {
}
