package ru.aisa.companyAndWorkers.repository;

import ru.aisa.companyAndWorkers.entity.Company;
import ru.aisa.companyAndWorkers.entity.Worker;

import java.util.List;
import java.util.Optional;

public interface WorkerRepository {

    List<Worker> findAll();

    List<Worker> findAllCompanyWorkers(Company company);

    Optional<Worker> findById (Long id);

    Optional<Worker> findByEmail(String email);

    Optional<Worker> save (Worker worker);

    Optional<Worker> update (Worker worker);

    void delete (Long id);

    void delete (String email);
}
