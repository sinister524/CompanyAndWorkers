package ru.aisa.companyAndWorkers.repository;

import ru.aisa.companyAndWorkers.entity.Company;

import java.util.List;
import java.util.Optional;

public interface CompanyRepository {

    List<Company> findAll();

    Optional<Company> findById (Long id);

    Optional<Company> findByInn (String inn);

    void delete (Long id);

    void delete (String inn);

    Optional<Company> save (Company company);

    Optional<Company> update (Company company);
}
