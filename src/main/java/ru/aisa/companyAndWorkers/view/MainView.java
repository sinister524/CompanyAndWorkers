package ru.aisa.companyAndWorkers.view;


import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import com.vaadin.ui.renderers.DateRenderer;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import ru.aisa.companyAndWorkers.entity.Company;
import ru.aisa.companyAndWorkers.entity.Worker;
import ru.aisa.companyAndWorkers.services.CompanyService;
import ru.aisa.companyAndWorkers.services.WorkerService;
import ru.aisa.companyAndWorkers.view.creation_forms.CreateOrEditCompanyForm;
import ru.aisa.companyAndWorkers.view.tabs.CompanyTab;
import ru.aisa.companyAndWorkers.view.tabs.GridTab;
import ru.aisa.companyAndWorkers.view.tabs.WorkerTab;

import javax.servlet.annotation.WebServlet;
import java.util.NoSuchElementException;

@Title("Companies and workers")
public class MainView extends UI {

    private HorizontalLayout companyButtonsLayout;
    private TabSheet tabSheet;

    @Override
    protected void init(VaadinRequest request) {
        try {
            VerticalLayout content = new VerticalLayout();

            initButtons();
            content.addComponent(companyButtonsLayout);
            content.setComponentAlignment(companyButtonsLayout, Alignment.MIDDLE_CENTER);

            initTabSheet();
            content.addComponent(tabSheet);
            content.setComponentAlignment(tabSheet, Alignment.MIDDLE_CENTER);

            setContent(content);
        } catch (CannotGetJdbcConnectionException e){
            Notification.show("Не удалось подключиться к базе данных", Notification.Type.ERROR_MESSAGE);
        }
    }

    private void initButtons(){
        this.companyButtonsLayout = new HorizontalLayout();

        Button createButton = new Button("Создать", clickEvent -> {
            GridTab selectedTab = (GridTab) tabSheet.getSelectedTab();
            selectedTab.create();
        });
        Button editButton = new Button("Редактировать", clickEvent -> {
            GridTab selectedTab = (GridTab) tabSheet.getSelectedTab();
            selectedTab.edit();
        });
        Button deleteButton = new Button("Удалить", clickEvent -> {
            GridTab selectedTab = (GridTab) tabSheet.getSelectedTab();
            selectedTab.delete();
        });
        this.companyButtonsLayout.addComponents(createButton, editButton, deleteButton);
    }

    private void initTabSheet(){
        this.tabSheet = new TabSheet();
        WorkerTab workerTab = new WorkerTab();
        CompanyTab companyTab = new CompanyTab(workerTab);
        this.tabSheet.addTab(companyTab, "Компании");
        this.tabSheet.addTab(workerTab, "Работники");
    }
}
