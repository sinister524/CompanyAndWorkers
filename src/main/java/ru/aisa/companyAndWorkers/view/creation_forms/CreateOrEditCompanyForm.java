package ru.aisa.companyAndWorkers.view.creation_forms;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import ru.aisa.companyAndWorkers.entity.Company;
import ru.aisa.companyAndWorkers.exception.NotUniqueValueException;
import ru.aisa.companyAndWorkers.services.CompanyService;

import java.util.NoSuchElementException;

@Log4j2
public class CreateOrEditCompanyForm extends Window {

    private CompanyService service;

    private VerticalLayout windowLayout;
    private HorizontalLayout nameLayout;
    private HorizontalLayout innLayout;
    private HorizontalLayout phoneLayout;
    private HorizontalLayout addressLayout;
    private HorizontalLayout buttonLayout;

    private Company company;
    private Binder<Company> binder;

    public CreateOrEditCompanyForm(Company company) {
        this.service = CompanyService.getInstance();
        this.company = company;
        this.windowLayout = new VerticalLayout();
        this.nameLayout = new HorizontalLayout();
        this.innLayout = new HorizontalLayout();
        this.phoneLayout = new HorizontalLayout();
        this.addressLayout = new HorizontalLayout();
        this.buttonLayout = new HorizontalLayout();

        this.binder = new Binder<>();
        initNameLayout();
        initInnLayout();
        initPhoneLayout();
        initAddressLayout();
        initButtonLayout();
        initWindowLayout();
        this.binder.readBean(company);
        this.setModal(true);
        this.setClosable(false);
        this.setResizable(false);
    }


    private void initNameLayout(){
        Label nameLabel = new Label("Название");
        this.nameLayout.addComponent(nameLabel);
        TextField nameField = new TextField();
        nameField.setPlaceholder("Название компании");
        nameField.setWidth("250px");
        this.binder.forField(nameField)
                .asRequired("Поле должно быть заполненно")
                .bind(Company::getName, Company::setName);
        this.nameLayout.addComponent(nameField);
    }

    private void initInnLayout(){
        Label innLabel = new Label("ИНН");
        this.innLayout.addComponent(innLabel);
        TextField innField = new TextField();
        innField.setPlaceholder("ИНН компании");
        this.binder.forField(innField)
                .asRequired("Поле должно быть заполненно")
                .withValidator(inn -> inn.length() == 10, "ИНН должен состоять из 10 цифр")
                .withValidator(inn -> inn.matches("-?\\d+(\\.\\d+)?"), "ИНН должен содержать только цифры")
                .bind(Company::getInn, Company::setInn);
        innField.setWidth("250px");
        this.innLayout.addComponent(innField);
    }

    private void initPhoneLayout(){
        Label phoneLabel = new Label("Телефон");
        this.phoneLayout.addComponent(phoneLabel);
        TextField phoneField = new TextField();
        phoneField.setPlaceholder("+79991112233");
        phoneField.setValue("+7");
        this.binder.forField(phoneField)
                .asRequired("Поле должно быть заполненно")
                .withValidator(phoneNumber -> phoneNumber.length() == 12,
                        "Телефонный номер должен состоять из 12 символов")
                .withValidator(phoneNumber -> phoneNumber.startsWith("+7")
                                && phoneNumber.substring(1, 11).matches("-?\\d+(\\.\\d+)?"),
                        "Номер телефона должен начиаться с +7 и содержать только цифры")
                .bind(Company::getPhoneNumber, Company::setPhoneNumber);
        phoneField.setWidth("250px");
        this.phoneLayout.addComponent(phoneField);
    }

    private void initAddressLayout(){
        Label addressLabel = new Label("Адресс");
        this.addressLayout.addComponent(addressLabel);
        TextField addressField = new TextField();
        addressField.setPlaceholder("Адресс компании");
        this. binder.forField(addressField)
                .asRequired("Поле должно быть заполненно")
                .bind(Company::getAddress, Company::setAddress);
        addressField.setWidth("250px");
        this. addressLayout.addComponent(addressField);
    }

    private void initButtonLayout(){
        Button saveButton = new Button("Сохранить", clickEvent -> {
            if (this.company.getId() == null){
                try {
                    this.binder.writeBean(company);
                    this.service.save(company);
                    close();
                    log.debug("Company " + company.getName() + " saved!");
                } catch (ValidationException e) {
                    Notification.show("Создание объекта не удалось! \n Поля заполненны неправильно",
                            Notification.Type.ERROR_MESSAGE);
                } catch (NotUniqueValueException e) {
                    Notification.show("Компания с таким ИНН уже существует", Notification.Type.ERROR_MESSAGE);
                }
            } else {
                try {
                    this.binder.writeBean(company);
                    this.service.update(company);
                    close();
                    log.debug("Company " + company.toString() + " updated!");
                } catch (ValidationException e) {
                    Notification.show("Редактирование объекта не удалось! \n Поля заполненны неправильно",
                            Notification.Type.ERROR_MESSAGE);
                } catch (NoSuchElementException e) {
                    Notification.show("Редактируемый элемент не существует", Notification.Type.ERROR_MESSAGE);
                } catch (NotUniqueValueException e) {
                    Notification.show("Компания с таким ИНН уже существует", Notification.Type.ERROR_MESSAGE);
                }
            }
        });
        this.binder.addStatusChangeListener(statusChangeEvent -> {
            boolean isValid = statusChangeEvent.getBinder().isValid();
            boolean hasChanges = statusChangeEvent.getBinder().hasChanges();
            saveButton.setEnabled(isValid && hasChanges);
        });

        Button cancelButton = new Button("Отмена", clickEvent -> close());
        this.buttonLayout.addComponents(saveButton, cancelButton);
    }

    private void initWindowLayout(){
        this.windowLayout.addComponent(nameLayout);
        this.windowLayout.setComponentAlignment(nameLayout, Alignment.MIDDLE_RIGHT);
        this.windowLayout.addComponent(innLayout);
        this.windowLayout.setComponentAlignment(innLayout, Alignment.MIDDLE_RIGHT);
        this.windowLayout.addComponent(phoneLayout);
        this.windowLayout.setComponentAlignment(phoneLayout, Alignment.MIDDLE_RIGHT);
        this.windowLayout.addComponent(addressLayout);
        this.windowLayout.setComponentAlignment(addressLayout, Alignment.MIDDLE_RIGHT);
        this.windowLayout.addComponent(buttonLayout);
        this.windowLayout.setComponentAlignment(buttonLayout, Alignment.MIDDLE_CENTER);
        this.setContent(windowLayout);
    }

}
