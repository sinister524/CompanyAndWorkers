package ru.aisa.companyAndWorkers.view.creation_forms;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.validator.DateRangeValidator;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.ui.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import ru.aisa.companyAndWorkers.entity.Company;
import ru.aisa.companyAndWorkers.entity.Worker;
import ru.aisa.companyAndWorkers.exception.NotUniqueValueException;
import ru.aisa.companyAndWorkers.services.CompanyService;
import ru.aisa.companyAndWorkers.services.WorkerService;

import java.time.LocalDate;
import java.util.NoSuchElementException;

@Log4j2
public class CreateOrEditWorkerForm extends Window {
    private WorkerService workerService;
    private CompanyService companyService;

    private VerticalLayout windowLayout;
    private HorizontalLayout nameLayout;
    private HorizontalLayout birthdayLayout;
    private HorizontalLayout emailLayout;
    private HorizontalLayout companyLayout;
    private HorizontalLayout buttonLayout;

    private Worker worker;
    private Binder<Worker> binder;

    public CreateOrEditWorkerForm(Worker worker) {
        this.workerService = WorkerService.getInstance();
        this.companyService = CompanyService.getInstance();
        this.worker = worker;
        this.windowLayout = new VerticalLayout();
        this.nameLayout = new HorizontalLayout();
        this.birthdayLayout = new HorizontalLayout();
        this.emailLayout = new HorizontalLayout();
        this.companyLayout = new HorizontalLayout();
        this.buttonLayout = new HorizontalLayout();

        this.binder = new Binder<>();
        initNameLayout();
        initBirthdayLayout();
        initEmailLayout();
        initCompanyLayout();
        initButtonLayout();
        initWindowLayout();
        this.binder.readBean(worker);
        this.setModal(true);
        this.setClosable(false);
        this.setResizable(false);
    }
    
    private void initNameLayout(){
        Label nameLabel = new Label("Имя");
        this.nameLayout.addComponent(nameLabel);
        TextField nameField = new TextField();
        nameField.setPlaceholder("Имя работника");
        this.binder.forField(nameField)
                .asRequired("Поле должно быть заполненно")
                .bind(Worker::getName, Worker::setName);
        nameField.setWidth("250px");
        this.nameLayout.addComponent(nameField);
    }

    private void initBirthdayLayout(){
        Label innLabel = new Label("Дата рождения");
        this.birthdayLayout.addComponent(innLabel);
        DateField birthdayField = new DateField();
        birthdayField.setDateFormat("dd.MM.yyyy");
        this.binder.forField(birthdayField)
                .asRequired("Поле должно быть заполненно")
                .withValidator(new DateRangeValidator("Дата рождения не может быть раньше 1900 года" +
                        " и позже чем за 16 лет от сегодня", LocalDate.of(1900, 1, 1),
                        LocalDate.now().minusYears(16)))
                .bind(Worker::getBirthdayInLocalDate, Worker::setBirthdayFromLocalDate);
        birthdayField.setWidth("250px");
        this.birthdayLayout.addComponent(birthdayField);
    }

    private void initEmailLayout(){
        Label emailLabel = new Label("Email");
        this.emailLayout.addComponent(emailLabel);
        TextField emailField = new TextField();
        emailField.setPlaceholder("example@domain.com");
        this.binder.forField(emailField)
                .asRequired("Поле должно быть заполненно")
                .withValidator(new EmailValidator("Строка не похожа на Email"))
                .bind(Worker::getEmail, Worker::setEmail);
        emailField.setWidth("250px");
        this.emailLayout.addComponent(emailField);
    }

    private void initCompanyLayout(){
        Label companyLabel = new Label("Компания");
        this.companyLayout.addComponent(companyLabel);
        ComboBox<Company> companyComboBox = new ComboBox<>();
        companyComboBox.setItems(companyService.findAll());
        this.binder.forField(companyComboBox)
                .asRequired("Работик должен быть приписан к компании")
                .bind(Worker::getCompany, Worker::setCompany);
        companyComboBox.setWidth("250px");
        this.companyLayout.addComponent(companyComboBox);
    }

    private void initButtonLayout(){
        Button saveButton = new Button("Сохранить", clickEvent -> {
            if (this.worker.getId() == null){
                try {
                    this.binder.writeBean(worker);
                    this.workerService.save(worker);
                    close();
                    log.debug("Worker " + worker.getName() + " saved!");
                } catch (ValidationException e) {
                    Notification.show("Создание объекта не удалось! \n Поля заполненны неправильно",
                            Notification.Type.ERROR_MESSAGE);
                } catch (NotUniqueValueException e) {
                    Notification.show("Работник с таким Email уже существует", Notification.Type.ERROR_MESSAGE);
                }
            } else {
                try {
                    this.binder.writeBean(worker);
                    this.workerService.update(worker);
                    close();
                    log.debug("Worker " + worker.getName() + " updated!");
                } catch (ValidationException e) {
                    Notification.show("Редактирование объекта не удалось! \n Поля заполненны неправильно",
                            Notification.Type.ERROR_MESSAGE);
                } catch (NoSuchElementException e) {
                    Notification.show("Редактируемый элемент не существует", Notification.Type.ERROR_MESSAGE);
                } catch (NotUniqueValueException e) {
                    Notification.show("Работник с таким Email уже существует", Notification.Type.ERROR_MESSAGE);
                }
            }
        });
        this.binder.addStatusChangeListener(statusChangeEvent -> {
            boolean isValid = statusChangeEvent.getBinder().isValid();
            boolean hasChanges = statusChangeEvent.getBinder().hasChanges();
            saveButton.setEnabled(isValid && hasChanges);
        });
        Button cancelButton = new Button("Отмена", clickEvent -> close());
        this.buttonLayout.addComponents(saveButton, cancelButton);
    }

    private void initWindowLayout(){
        this.windowLayout.addComponent(nameLayout);
        this.windowLayout.setComponentAlignment(nameLayout, Alignment.MIDDLE_RIGHT);
        this.windowLayout.addComponent(birthdayLayout);
        this.windowLayout.setComponentAlignment(birthdayLayout, Alignment.MIDDLE_RIGHT);
        this.windowLayout.addComponent(emailLayout);
        this.windowLayout.setComponentAlignment(emailLayout, Alignment.MIDDLE_RIGHT);
        this.windowLayout.addComponent(companyLayout);
        this.windowLayout.setComponentAlignment(companyLayout, Alignment.MIDDLE_RIGHT);
        this.windowLayout.addComponent(buttonLayout);
        this.windowLayout.setComponentAlignment(buttonLayout, Alignment.MIDDLE_CENTER);
        this.setContent(windowLayout);
    }

}
