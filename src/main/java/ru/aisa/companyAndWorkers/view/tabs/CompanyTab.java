package ru.aisa.companyAndWorkers.view.tabs;

import com.vaadin.ui.*;
import lombok.extern.log4j.Log4j2;
import ru.aisa.companyAndWorkers.entity.Company;
import ru.aisa.companyAndWorkers.services.CompanyService;
import ru.aisa.companyAndWorkers.view.creation_forms.CreateOrEditCompanyForm;

import java.util.NoSuchElementException;

@Log4j2
public class CompanyTab extends VerticalLayout implements GridTab {

    private final CompanyService companyService;
    private Grid<Company> companyGrid;

    private final WorkerTab workerTab;

    public CompanyTab(WorkerTab workerTab) {
        this.companyService = CompanyService.getInstance();
        this.workerTab = workerTab;
        initCompanyGrid();
        this.addComponent(companyGrid);
        this.setComponentAlignment(companyGrid, Alignment.MIDDLE_CENTER);
    }

    private void initCompanyGrid() {
        companyGrid = new Grid<>();
        companyGrid.setItems(companyService.findAll());
        companyGrid.addColumn(Company::getName).setCaption("Название организации");
        companyGrid.addColumn(Company::getInn).setCaption("ИНН");
        companyGrid.addColumn(Company::getPhoneNumber).setCaption("Номер телефона");
        companyGrid.addColumn(Company::getAddress).setCaption("Адрес");
        companyGrid.setWidthFull();
        companyGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        companyGrid.addSelectionListener(selectionEvent -> {
            try {
                Company selectedCompany = selectionEvent.getFirstSelectedItem().orElseThrow(NoSuchElementException::new);
                workerTab.fillGridByCompanyWorkers(selectedCompany);
            } catch (NoSuchElementException e){
                workerTab.refreshWorkers();
            }
        });
    }

    @Override
    public void create() {
        CreateOrEditCompanyForm createForm = new CreateOrEditCompanyForm(new Company());
        createForm.addCloseListener(closeEvent -> companyGrid.setItems(companyService.findAll()));
        UI.getCurrent().addWindow(createForm);
    }

    @Override
    public void edit() {
        try {
            Company selectedCompany = companyGrid.getSelectedItems().stream().findFirst().orElseThrow(NoSuchElementException::new);
            CreateOrEditCompanyForm createForm = new CreateOrEditCompanyForm(selectedCompany);
            createForm.addCloseListener(closeEvent -> companyGrid.setItems(companyService.findAll()));
            UI.getCurrent().addWindow(createForm);
        } catch (NoSuchElementException e){
            Notification.show("Выберите элемент для редактирования", Notification.Type.WARNING_MESSAGE);
        }
    }

    @Override
    public void delete() {
        try {
            Company selectedCompany = companyGrid.getSelectedItems().stream().findFirst().orElseThrow(NoSuchElementException::new);
            UI.getCurrent().addWindow(getDeleteCompanyDialogWindow(selectedCompany));
        } catch (NoSuchElementException e){
            Notification.show("Выберите элемент для удаления", Notification.Type.WARNING_MESSAGE);
        }
    }

    private Window getDeleteCompanyDialogWindow(Company company){
        Window deleteDialogWindow = new Window();
        VerticalLayout content = new VerticalLayout();
        Label message = new Label("Вы действительно хотиту удалить компанию " + company.getName());
        content.addComponent(message);
        content.setComponentAlignment(message, Alignment.MIDDLE_CENTER);

        HorizontalLayout buttonLayout = new HorizontalLayout();
        Button delete = new Button("Удалить", clickEvent -> {
            try {
                companyService.delete(company.getId());
                companyGrid.deselectAll();
                companyGrid.setItems(companyService.findAll());
                deleteDialogWindow.close();
                Notification.show("Компания " + company.getName() + " удалена!", Notification.Type.WARNING_MESSAGE);
                log.debug("Company " + company.toString() + " deleted!");
            } catch (NoSuchElementException e) {
                deleteDialogWindow.close();
                companyGrid.setItems(companyService.findAll());
                Notification.show("Удаляемый элемент не существует", Notification.Type.ERROR_MESSAGE);
            }

        });
        Button cancel = new Button("Отмена", clickEvent -> deleteDialogWindow.close());
        buttonLayout.addComponents(delete, cancel);
        content.addComponent(buttonLayout);
        content.setComponentAlignment(buttonLayout, Alignment.MIDDLE_CENTER);

        deleteDialogWindow.setContent(content);
        deleteDialogWindow.setModal(true);
        deleteDialogWindow.setClosable(false);
        deleteDialogWindow.setResizable(false);
        deleteDialogWindow.setDraggable(false);

        return deleteDialogWindow;
    }
}
