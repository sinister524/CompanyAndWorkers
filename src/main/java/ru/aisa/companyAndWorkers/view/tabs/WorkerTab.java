package ru.aisa.companyAndWorkers.view.tabs;

import com.vaadin.ui.*;
import com.vaadin.ui.renderers.DateRenderer;
import lombok.extern.log4j.Log4j2;
import ru.aisa.companyAndWorkers.entity.Company;
import ru.aisa.companyAndWorkers.entity.Worker;
import ru.aisa.companyAndWorkers.services.WorkerService;
import ru.aisa.companyAndWorkers.view.creation_forms.CreateOrEditWorkerForm;

import java.util.NoSuchElementException;

@Log4j2
public class WorkerTab extends VerticalLayout implements GridTab {

    private final WorkerService workerService;

    private Grid<Worker> workerGrid;

    public WorkerTab() {
        this.workerService = WorkerService.getInstance();
        initWorkerGrid();
        this.addComponent(workerGrid);
        this.setComponentAlignment(workerGrid, Alignment.MIDDLE_CENTER);
    }

    private void initWorkerGrid() {
        workerGrid = new Grid<>();
        workerGrid.setItems(workerService.findAll());
        workerGrid.addColumn(Worker::getName).setCaption("Имя");
        workerGrid.addColumn(Worker::getBirthday, new DateRenderer("%1$td.%1$tm.%1$tY")).setCaption("Дата рождения");
        workerGrid.addColumn(Worker::getEmail).setCaption("Email");
        workerGrid.addColumn(worker -> worker.getCompany().getName()).setCaption("Компания");
        workerGrid.setWidthFull();
        workerGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
    }

    @Override
    public void create() {
        CreateOrEditWorkerForm createForm = new CreateOrEditWorkerForm(new Worker());
        createForm.addCloseListener(closeEvent -> workerGrid.setItems(workerService.findAll()));
        UI.getCurrent().addWindow(createForm);
    }

    @Override
    public void edit() {
        try {
            Worker selectedWorker = workerGrid.getSelectedItems().stream().findFirst().orElseThrow(NoSuchElementException::new);
            CreateOrEditWorkerForm editForm = new CreateOrEditWorkerForm(selectedWorker);
            editForm.addCloseListener(closeEvent -> workerGrid.setItems(workerService.findAll()));
            UI.getCurrent().addWindow(editForm);
        } catch (NoSuchElementException e){
            Notification.show("Выберите элемент для редактирования", Notification.Type.WARNING_MESSAGE);
        }
    }

    @Override
    public void delete() {
        try {
            Worker selectedWorker = workerGrid.getSelectedItems().stream().findFirst().orElseThrow(NoSuchElementException::new);
            UI.getCurrent().addWindow(getDeleteWorkerDialogWindow(selectedWorker));
        } catch (NoSuchElementException e){
            Notification.show("Выберите элемент для удаления", Notification.Type.WARNING_MESSAGE);
        }
    }

    private Window getDeleteWorkerDialogWindow(Worker worker){
        Window deleteDialogWindow = new Window();
        VerticalLayout content = new VerticalLayout();
        Label message = new Label("Вы действительно хотиту удалить работника " + worker.getName());
        content.addComponent(message);
        content.setComponentAlignment(message, Alignment.MIDDLE_CENTER);

        HorizontalLayout buttonLayout = new HorizontalLayout();
        Button delete = new Button("Удалить", clickEvent -> {
            try {
                workerService.delete(worker.getId());
                workerGrid.deselectAll();
                workerGrid.setItems(workerService.findAll());
                deleteDialogWindow.close();
                Notification.show("Работник " + worker.getName() + " удален!", Notification.Type.WARNING_MESSAGE);
                log.debug("Worker " + worker.getName() + " deleted!");
            } catch (NoSuchElementException e) {
                deleteDialogWindow.close();
                workerGrid.setItems(workerService.findAll());
                Notification.show("Удаляемый элемент не существует", Notification.Type.ERROR_MESSAGE);
            }

        });
        Button cancel = new Button("Отмена", clickEvent -> deleteDialogWindow.close());
        buttonLayout.addComponents(delete, cancel);
        content.addComponent(buttonLayout);
        content.setComponentAlignment(buttonLayout, Alignment.MIDDLE_CENTER);

        deleteDialogWindow.setContent(content);
        deleteDialogWindow.setModal(true);
        deleteDialogWindow.setClosable(false);
        deleteDialogWindow.setResizable(false);
        deleteDialogWindow.setDraggable(false);

        return deleteDialogWindow;
    }

    public void refreshWorkers(){
        workerGrid.setItems(workerService.findAll());
    }

    public void fillGridByCompanyWorkers(Company company) {
        workerGrid.setItems(workerService.findAllCompanyWorkers(company));
    }
}
