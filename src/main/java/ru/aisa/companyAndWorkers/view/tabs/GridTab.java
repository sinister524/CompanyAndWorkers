package ru.aisa.companyAndWorkers.view.tabs;

public interface GridTab {

    void create();

    void edit();

    void delete();

}
