package ru.aisa.companyAndWorkers.row_mapper;


import org.springframework.jdbc.core.RowMapper;
import ru.aisa.companyAndWorkers.entity.Company;
import ru.aisa.companyAndWorkers.entity.Worker;

import java.sql.ResultSet;
import java.sql.SQLException;


public class WorkerRowMapper implements RowMapper<Worker> {

    private static WorkerRowMapper instance;

    private WorkerRowMapper() {
    }

    public static WorkerRowMapper getInstance() {
        if (instance == null){
            instance = new WorkerRowMapper();
        }
        return instance;
    }

    @Override
    public Worker mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Worker(resultSet.getLong("id"), resultSet.getString("name"), resultSet.getDate("birthday"),
                resultSet.getString("email"),
                new Company(
                        resultSet.getLong("company_id"),
                        resultSet.getString("company_name"),
                        resultSet.getString("company_inn"),
                        resultSet.getString("company_phone_number"),
                        resultSet.getString("company_address")
                ));
    }
}
