package ru.aisa.companyAndWorkers.dao;

import lombok.extern.log4j.Log4j2;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import ru.aisa.companyAndWorkers.comfig.SpringJdbcConfig;
import ru.aisa.companyAndWorkers.entity.Company;
import ru.aisa.companyAndWorkers.entity.Worker;
import ru.aisa.companyAndWorkers.repository.WorkerRepository;
import ru.aisa.companyAndWorkers.row_mapper.WorkerRowMapper;

import java.util.List;
import java.util.Optional;

@Log4j2
public class WorkerDao implements WorkerRepository {

    private static WorkerDao instance;

    private final WorkerRowMapper workerRowMapper;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private WorkerDao() {
        this.workerRowMapper = WorkerRowMapper.getInstance();
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(
                new JdbcTemplate(SpringJdbcConfig.getInstance().postgreDataSource()));
    }

    public static synchronized WorkerDao getInstance() {
        if (instance == null) {
            instance = new WorkerDao();
        }
        return instance;
    }

    private final String FIND_ALL_QUERY = "SELECT public.worker.*, " +
            "public.company.name AS company_name, public.company.inn AS company_inn, " +
            "public.company.phone_number AS company_phone_number, public.company.address AS company_address " +
            "FROM public.worker JOIN public.company ON (public.worker.company_id = public.company.id)";
    @Override
    public List<Worker> findAll() {
        log.debug("Load add workers from DB");
        return namedParameterJdbcTemplate.query(FIND_ALL_QUERY, workerRowMapper);
    }

    private final String FIND_ALL_COMPANY_WORKERS_QUERY = "SELECT public.worker.*, " +
            "public.company.name AS company_name, public.company.inn AS company_inn, " +
            "public.company.phone_number AS company_phone_number, public.company.address AS company_address " +
            "FROM public.worker JOIN public.company ON (public.worker.company_id = public.company.id) " +
            "WHERE public.worker.company_id = ";
    @Override
    public List<Worker> findAllCompanyWorkers(Company company) {
        log.debug("Load all Company - " + company.toString() + " workers from DB");
        return namedParameterJdbcTemplate.query(
                FIND_ALL_COMPANY_WORKERS_QUERY + company.getId(),
                workerRowMapper);
    }

    private final String FIND_BY_ID_QUERY = "SELECT public.worker.*, " +
            "public.company.name AS company_name, public.company.inn AS company_inn, " +
            "public.company.phone_number AS company_phone_number, public.company.address AS company_address " +
            "FROM public.worker JOIN public.company ON (public.worker.company_id = public.company.id) " +
            "WHERE public.worker.id = :id";
    @Override
    public Optional<Worker> findById(Long id) {
        log.debug("Load Worker by ID = " + id + " from DB");
        SqlParameterSource requiredId = new MapSqlParameterSource().addValue("id", id);
        Optional<Worker> optionalWorker;
        try {
            optionalWorker = Optional.ofNullable(
                    namedParameterJdbcTemplate.queryForObject(FIND_BY_ID_QUERY, requiredId, workerRowMapper));
            log.debug("Worker with ID = " + id + " founded");
        } catch (EmptyResultDataAccessException e){
            optionalWorker = Optional.empty();
            log.debug("Company with ID = " + id + " not found");
        }
        return optionalWorker;
    }

    private final String FIND_BY_EMAIL_QUERY = "SELECT public.worker.*, " +
            "public.company.name AS company_name, public.company.inn AS company_inn, " +
            "public.company.phone_number AS company_phone_number, public.company.address AS company_address " +
            "FROM public.worker JOIN public.company ON (public.worker.company_id = public.company.id) " +
            "WHERE public.worker.email = :email";
    @Override
    public Optional<Worker> findByEmail(String email) {
        log.debug("Load Worker by email = " + email + " from DB");
        SqlParameterSource requiredEmail = new MapSqlParameterSource().addValue("email", email);
        Optional<Worker> optionalWorker;
        try {
            optionalWorker = Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(FIND_BY_EMAIL_QUERY,
                    requiredEmail,workerRowMapper));
            log.debug("Worker with email = " + email + " founded");
        } catch (EmptyResultDataAccessException e){
            log.debug("Worker with email = " + email + " not found");
            optionalWorker = Optional.empty();
        }
        return optionalWorker;
    }

    private final String SAVE_QUERY = "INSERT INTO public.worker (name, birthday, email, company_id) " +
            "VALUES (:name, :birthday, :email, :company_id)";
    @Override
    public Optional<Worker> save(Worker worker) {
        log.debug("Try to save new worker - " + worker.getName() + " in DB");
        SqlParameterSource parametersForInsert = new MapSqlParameterSource()
                .addValue("name", worker.getName())
                .addValue("birthday", worker.getBirthday())
                .addValue("email", worker.getEmail())
                .addValue("company_id", worker.getCompany().getId());
        namedParameterJdbcTemplate.update(SAVE_QUERY, parametersForInsert);
        return findByEmail(worker.getEmail());
    }

    private final String UPDATE_QUERY = "UPDATE public.worker " +
            "SET name = :name, birthday = :birthday, email =:email, company_id = :company_id " +
            "WHERE id = :id";
    @Override
    public Optional<Worker> update(Worker worker) {
        log.debug("Try to update worker - " + worker.toString() + " in DB");
        SqlParameterSource parametersForUpdate = new MapSqlParameterSource()
                .addValue("id", worker.getId())
                .addValue("name", worker.getName())
                .addValue("birthday", worker.getBirthday())
                .addValue("email", worker.getEmail())
                .addValue("company_id", worker.getCompany().getId());
        namedParameterJdbcTemplate.update(UPDATE_QUERY, parametersForUpdate);
        return findById(worker.getId());
    }

    private final String DELETE_BY_ID_QUERY = "DELETE FROM public.worker WHERE id = :id";
    @Override
    public void delete(Long id) {
        log.debug("Try to delete worker with ID = " + id + " in DB");
        SqlParameterSource requiredIdParameter = new MapSqlParameterSource().addValue("id", id);
        namedParameterJdbcTemplate.update(DELETE_BY_ID_QUERY, requiredIdParameter);
    }

    private final String DELETE_BY_EMAIL_QUERY = "DELETE FROM public.worker WHERE email = :email";
    @Override
    public void delete(String email) {
        log.debug("Try to delete worker with email = " + email + " in DB");
        SqlParameterSource requiredEmailParameter = new MapSqlParameterSource().addValue("email", email);
        namedParameterJdbcTemplate.update(DELETE_BY_EMAIL_QUERY, requiredEmailParameter);
    }
}
