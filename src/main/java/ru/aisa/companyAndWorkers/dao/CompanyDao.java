package ru.aisa.companyAndWorkers.dao;


import lombok.extern.log4j.Log4j2;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import ru.aisa.companyAndWorkers.comfig.SpringJdbcConfig;
import ru.aisa.companyAndWorkers.entity.Company;
import ru.aisa.companyAndWorkers.repository.CompanyRepository;
import ru.aisa.companyAndWorkers.row_mapper.CompanyRowMapper;

import java.util.List;
import java.util.Optional;

@Log4j2
public class CompanyDao implements CompanyRepository {

    private static CompanyDao instance;

    private final CompanyRowMapper companyRowMapper;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private CompanyDao() {
        this.companyRowMapper = CompanyRowMapper.getInstance();
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(
                new JdbcTemplate(SpringJdbcConfig.getInstance().postgreDataSource()));
    }

    public static synchronized CompanyDao getInstance() {
        if (instance == null) {
            instance = new CompanyDao();
        }
        return instance;
    }

    private final String FIND_ALL_QUERY = "SELECT * FROM public.company";
    @Override
    public List<Company> findAll() {
        log.debug("Load all Companies from DB");
        return namedParameterJdbcTemplate.query(FIND_ALL_QUERY, companyRowMapper);
    }

    private final String FIND_BY_ID_QUERY = "SELECT * FROM public.company WHERE id = :id";
    @Override
    public Optional<Company> findById(Long id) {
        log.debug("Load Company by ID = " + id + " from DB");
        SqlParameterSource requiredId = new MapSqlParameterSource().addValue("id", id);
        Optional<Company> optionalCompany;
        try {
            optionalCompany = Optional.ofNullable(
                    namedParameterJdbcTemplate.queryForObject(FIND_BY_ID_QUERY, requiredId, companyRowMapper));
            log.debug("Company with ID = " + id + " founded");
        } catch (EmptyResultDataAccessException e){
            optionalCompany = Optional.empty();
            log.debug("Company with ID = " + id + " not found");
        }
        return optionalCompany;
    }

    private final String FIND_BY_INN_QUERY = "SELECT * FROM public.company WHERE inn = :inn";
    @Override
    public Optional<Company> findByInn(String inn) {
        log.debug("Load Company by INN = " + inn + " from BB");
        SqlParameterSource requiredInnParameter = new MapSqlParameterSource().addValue("inn", inn);
        Optional<Company> optionalCompany;
        try {
            optionalCompany = Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(FIND_BY_INN_QUERY, requiredInnParameter,
                    companyRowMapper));
            log.debug("Company with INN = " + inn + " founded");
        } catch (EmptyResultDataAccessException e) {
            optionalCompany = Optional.empty();
            log.debug("Company with INN = " + inn + " not found");
        }
        return optionalCompany;
    }

    private final String SAVE_QUERY = "INSERT INTO public.company (name, inn, phone_number, address) " +
            "VALUES (:name, :inn, :phone_number, :address)";
    @Override
    public Optional<Company> save(Company company) {
        log.debug("Try to save new company - " + company.getName() + " in DB");
        SqlParameterSource companyParametersForInsert = new MapSqlParameterSource()
                .addValue("name", company.getName())
                .addValue("inn", company.getInn())
                .addValue("phone_number", company.getPhoneNumber())
                .addValue("address", company.getAddress());

        namedParameterJdbcTemplate.update(SAVE_QUERY, companyParametersForInsert);
        return findByInn(company.getInn());
    }

    private final String UPDATE_QUERY = "UPDATE public.company " +
            "SET name = :name, inn = :inn, phone_number = :phone_number, address = :address WHERE id = :id";
    @Override
    public Optional<Company> update(Company company) {
        log.debug("Try to update Company - " + company.toString() + " in DB");
        SqlParameterSource companyParametersForUpdate = new MapSqlParameterSource()
                .addValue("id", company.getId())
                .addValue("name", company.getName())
                .addValue("inn", company.getInn())
                .addValue("phone_number", company.getPhoneNumber())
                .addValue("address", company.getAddress());
        namedParameterJdbcTemplate.update(UPDATE_QUERY, companyParametersForUpdate);

        return findById(company.getId());
    }

    private final String DELETE_BY_ID_QUERY = "DELETE FROM public.company WHERE id = :id";
    @Override
    public void delete(Long id) {
        log.debug("Try to delete company with ID = " + id + " in DB");
        SqlParameterSource requiredIdParameter = new MapSqlParameterSource().addValue("id", id);
        namedParameterJdbcTemplate.update(DELETE_BY_ID_QUERY, requiredIdParameter);
    }

    private final String DELETE_BY_INN_QUERY = "DELETE FROM public.company WHERE inn = :inn";
    @Override
    public void delete(String inn) {
        log.debug("Try to delete company with INN = " + inn + " in DB");
        SqlParameterSource requiredInnParameter = new MapSqlParameterSource().addValue("inn", inn);
        namedParameterJdbcTemplate.update(DELETE_BY_INN_QUERY, requiredInnParameter);
    }
}
